﻿using PoliceDatabase.Person;
using System;
using System.Collections.Generic;

namespace PoliceDatabase
{
    class Program
    {
        public static string CURRENTLY_LOGGED_IN = "None";

        public static List<Officer> Officers = new List<Officer>();

        static void Main(string[] args)
        {

            Console.WindowWidth = 140;
            /* Creating officers */
            //Heslo Officer1
            Officer of1 = new Officer("officer1", "68564b54e9796be70d34636e786c09d6bc09b37e1adb59bb3f62268f73857496");
            //Heslo Officer2
            Officer of2 = new Officer("officer2", "45a3eb6c46c9ef497c34998cb2447f740707be99f00fed2de15abbf671217e10");
            //Heslo Officer3
            Officer of3 = new Officer("officer3", "3a31ff61ebe34e6fe286db5cec50c0dff344f041bb5596279c3eb30a82ab16e4");
            //Heslo o1
            Officer o1 = new Officer("o1", "2352da7280f1decc3acf1ba84eb945c9fc2b7b541094e1d0992dbffd1b6664cc");

            Officers.Add(of1);
            Officers.Add(of2);
            Officers.Add(of3);
            Officers.Add(o1);


            /* //Creating officers */

            /* TESTing wanted Person */

            WantedPerson wantedPerson = new WantedPerson("Honza","Kozel",Sex.Male,"Opletalova 14, Praha", "CZE", new DateTime(2000, 6, 1), 23, 28, new DateTime(2020, 6, 1), "Knife");
            WantedPerson wantedPerson2 = new WantedPerson("Ilona","Stará",Sex.Female,"Zákopová 90, Kroměříž", "CZE", new DateTime(1950, 1, 12), 75, 80, new DateTime(2022, 6, 1), "");
            WantedPerson wantedPerson3 = new WantedPerson("Honza","Kozel",Sex.Male,"Opletalova 14, Praha", "CZE", new DateTime(2000, 6, 1), 23, 28, new DateTime(2020, 6, 1), "Knife");
            /* TESTing Missing Person */

            MissingPerson missingPerson = new MissingPerson("Karel", "Nerjedlý", Sex.Male, "Soukalova 78, Brno", "CZE", new DateTime(2001, 5, 12), 20, 25, new DateTime(2020, 6, 1));
            MissingPerson missingPerson2 = new MissingPerson("Veronika", "Kulíková", Sex.Female, "Fibichova 1847, Neratovice", "CZE", new DateTime(1942, 11, 19), 75, 85, new DateTime(2021, 1, 1));
            MissingPerson missingPerson3 = new MissingPerson("Boleslav", "Dráb", Sex.Male, "Osvobození 1333, Rychvald", "CZE", new DateTime(1990, 9, 5), 30, 34, new DateTime(2019, 12, 8));


            Menu.MainMenu();

        }
        
    }
}
