﻿using PoliceDatabase.Person;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PoliceDatabase;
using ConsoleTables;

namespace PoliceDatabase
{
    class Database
    {
        public static void DatabaseMenu()
        {
            Console.Clear();
            Render.Header();
            Render.LoggedIn();

            int selectedDatabaseMenu = ConsoleHelper.MultipleChoice(false, "1) Add wanted or missing person","2) Remove wanted or missing person", "3) Search for wanted criminals", "4) Search for missing persons", "5) Save wanted criminals","6) Save missing persons", "7) Load wanted criminals", "8) Load missing persons");


            Render.Footer();


            switch (selectedDatabaseMenu)
            {
                case 0:
                    AddMissingOrWanted();
                    break;
                case 1:
                    RemoveMissingOrWanted();
                    break;
                case 2:
                    OutputWantedPerson();
                   
                    break;
                case 3:
                    OutputMissingPerson();
                   
                    break;
                case 4:
                    SaveWantedPerson();
                   
                    break;
                case 5:
                    SaveMissingPerson();
                   
                    break;
                case 6:
                    LoadWantedPerson();
                    break;
                case 7:
                    LoadMissingPerson();
                    break;
                default:
                    break;
            }

            

        }
        public static void AddMissingOrWanted()
        {
            Console.Clear();
            Render.Header();
            Render.LoggedIn();

            char inputChar;

            Console.Write("Add Missing(m) or Wanted(w) person? (m/w)");
            inputChar = Console.ReadKey().KeyChar;
            Console.WriteLine();

            if (Char.ToLower(inputChar) == 'm')
            {
                //TODO: Add missing person
                MissingPerson.InputMissingPerson(MissingPerson.InputPerson());

                Render.Header("Missing person successfully added to database!");
                Console.ReadKey();
                Database.DatabaseMenu();


            }
            else if (Char.ToLower(inputChar) == 'w')
            {
                WantedPerson.InputWantedPerson(WantedPerson.InputPerson());

                Render.Header("Wanted person successfully added to database!");
                Console.ReadKey();
                Database.DatabaseMenu();
            }
            else
            {
                //ERROR

            }



        }
        public static void OutputWantedPerson()
        {
            Console.Clear();
            Render.Header();

            WantedPerson.GetTable().Write();


            Console.ReadKey();
            DatabaseMenu();

        }
        public static void OutputMissingPerson()
        {
            Console.Clear();
            Render.Header();

            MissingPerson.GetTable().Write();

            Console.ReadKey();
            DatabaseMenu();
        }
        public static void SaveWantedPerson() {
            Console.Clear();
            Render.Header();
            Render.Header("Save wanted criminals as XML");

            WantedPerson.GetTable().Write();

            
            ConsoleKey response;
            do
            {
                Console.Write("Do you want to save wanted criminals? [y/n] ");
                response = Console.ReadKey(false).Key;  
                if (response != ConsoleKey.Enter)
                    Console.WriteLine();

            } while (response != ConsoleKey.Y && response != ConsoleKey.N);
            if (response == ConsoleKey.Y)
            {
                do
                {
                    Console.Write("Do you want to also make encrypted version of the file? [y/n] ");
                    response = Console.ReadKey(false).Key;
                    if (response != ConsoleKey.Enter)
                        Console.WriteLine();

                } while (response != ConsoleKey.Y && response != ConsoleKey.N);
                if (response == ConsoleKey.Y)
                {
                    int password;
                    Console.Write("Input your password (numbers): ");
                    while (!int.TryParse(Console.ReadLine(), out password))
                        Console.Write("Incorrect input! \n" + "Input your password (numbers): ");
                    try
                    {
                        WantedPerson.OutputXMLEncrypted(password);
                    }
                    catch (Exception)
                    {
                        Render.Header("Encryption failed");
                        Console.ReadKey();
                        DatabaseMenu();

                       throw;
                    }
                   


                    Render.Header("Wanted criminals saved successfully with encryption! Used password: "+password.ToString());

                    Console.ReadKey();
                    DatabaseMenu();
                }
                else
                {
                    WantedPerson.OutputXML();
                    Console.WriteLine("Done");
                    Render.Header("Wanted criminals saved successfully!");

                    Console.ReadKey();
                    DatabaseMenu();
                }
            }
            else
            {
                DatabaseMenu();
            }
        }
        public static void SaveMissingPerson()
        {
            Console.Clear();
            Render.Header();
            Render.Header("Save missing persons as XML");

            MissingPerson.GetTable().Write();


            ConsoleKey response;
            do
            {
                Console.Write("Do you want to save missing persons? [y/n] ");
                response = Console.ReadKey(false).Key;
                if (response != ConsoleKey.Enter)
                    Console.WriteLine();

            } while (response != ConsoleKey.Y && response != ConsoleKey.N);
            if (response == ConsoleKey.Y)
            {
                MissingPerson.OutputXML();
                Render.Header("Missing criminals saved successfully!");

                Console.ReadKey();
                DatabaseMenu();
            }
            else
            {
                DatabaseMenu();
            }
        }
        public static void LoadWantedPerson()
        {
            Console.Clear();
            Render.Header();
            Render.Header("Load wanted criminals from XML");

            Console.WriteLine("Current wanted criminals in database:");
            WantedPerson.GetTable().Write();

            ConsoleKey response;
            do
            {
                Console.Write("Do you want to load wanted criminals? [y/n] ");
                response = Console.ReadKey(false).Key;
                if (response != ConsoleKey.Enter)
                    Console.WriteLine();

            } while (response != ConsoleKey.Y && response != ConsoleKey.N);
            if (response == ConsoleKey.Y)
            {
                do
                {
                    Console.Write("Is the file encrypted? [y/n] ");
                    response = Console.ReadKey(false).Key;
                    if (response != ConsoleKey.Enter)
                        Console.WriteLine();

                } while (response != ConsoleKey.Y && response != ConsoleKey.N);
                if (response == ConsoleKey.Y)
                {
                    int password;
                    Console.Write("Input your password (numbers): ");
                    while (!int.TryParse(Console.ReadLine(), out password))
                        Console.Write("Incorrect input! \n" + "Input your password (numbers): ");

                    try
                    {
                        WantedPerson.LoadXMLEncrypted(password);
                    }
                    catch (Exception)
                    {
                        Render.Header("Decryption failed, make sure that file is in ../LOAD_XML/ directory and is named WantedCriminals.encrypted. Also password could be wrong");
                        Console.ReadKey();
                        DatabaseMenu();
                        throw;
                    }


                    Render.Header("Wanted criminals loaded successfully and have been Decrypted! Used password: " + password.ToString());

                    Console.WriteLine("New wanted criminals in database:");
                    WantedPerson.GetTable().Write();

                    Console.ReadKey();
                    DatabaseMenu();
                }
                else
                {
                    WantedPerson.LoadXML();
                    Console.WriteLine("Done");
                    Render.Header("Wanted criminals loaded successfully!");

                    Console.WriteLine("New wanted criminals in database:");
                    WantedPerson.GetTable().Write();

                    Console.ReadKey();
                    DatabaseMenu();
                }
            }
            else
            {
                DatabaseMenu();
            }



        }
        public static void RemoveMissingOrWanted()
        {
            Console.Clear();
            Render.Header();
            Render.Header("Remove wanted criminal or missing person using their ID number");

            Console.WriteLine("Wanted criminals:");
            WantedPerson.GetTable().Write();

            Console.WriteLine("Missing persons:");
            MissingPerson.GetTable().Write();

            ConsoleKey response;
            do
            {
                Console.Write("Do you want to remove wanted or missing person? [y/n] ");
                response = Console.ReadKey(false).Key;
                if (response != ConsoleKey.Enter)
                    Console.WriteLine();

            } while (response != ConsoleKey.Y && response != ConsoleKey.N);
            if (response == ConsoleKey.Y)
            {
                int remove;
                Console.Write("Enter ID of wanted or missing person to remove it from database: ");
                
                while (!(int.TryParse(Console.ReadLine(), out remove) && remove <= Identification.ID))
                    Console.Write("Incorrect input! \n" + "Enter ID of wanted or missing person to remove it from database: [-1 to return back]");

                if (remove <= -1)
                {
                    DatabaseMenu();
                }

                

                foreach (var missingPerson in MissingPerson.MissingPersons)
                {
                    if (missingPerson.ID == remove)
                    {
                        MissingPerson.MissingPersons.Remove(missingPerson);
                        Render.Header("Removed Missing person with ID:"+ missingPerson.ID);
                    }
                }
                foreach (var wantedPerson in WantedPerson.WantedPersons)
                {
                    if (wantedPerson.ID == remove)
                    {
                        WantedPerson.WantedPersons.Remove(wantedPerson);
                        Render.Header("Removed wanted person with ID:" + wantedPerson.ID);
                    }
                }
            }







        }
        public static void LoadMissingPerson() {
            Console.Clear();
            Render.Header();
            Render.Header("Load missing persons from XML");

            Console.WriteLine("Current missidng persons in database:");
            MissingPerson.GetTable().Write();


            ConsoleKey response;
            do
            {
                Console.Write("Do you want to load missing persons? [y/n] ");
                response = Console.ReadKey(false).Key;
                if (response != ConsoleKey.Enter)
                    Console.WriteLine();

            } while (response != ConsoleKey.Y && response != ConsoleKey.N);
            if (response == ConsoleKey.Y) {
                MissingPerson.LoadXML();
                Console.WriteLine("Done");
                Render.Header("Missing persons loaded successfully!");

                Console.WriteLine("New missing peersons in database:");
                MissingPerson.GetTable().Write();

                Console.ReadKey();
                DatabaseMenu();
            }
            else
            {
                DatabaseMenu();
            }

        }



    }
}
