using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

//TO DO: add birth date!

namespace PoliceDatabase.Person
{
    public static class Identification {
        public static int ID = 0;
    }
    public enum Sex
    {
        Male,
        Female,
        Undefined
    }
     public class Person
    {
        public int ID = Identification.ID;
        public string Name { get; set; }
        public string Surname { get; set; }
        public Sex Sex { get; set; }
        public string Address { get; set; }
        public string Nationality { get; set; }
        public DateTime BirthDate { get; set; }
        public int MinAge { get; set; }
        public int MaxAge { get; set; }

        public Person(string name, string surname, Sex sex, string address,DateTime birthDate, string nationality, int minAge, int maxAge)
        {
            ID = generateID();
            this.Name = name;
            this.Surname = surname;
            this.Sex = sex;
            this.Address = address;
            this.Nationality = nationality;
            this.BirthDate = birthDate;
            this.MinAge = minAge;
            this.MaxAge = maxAge;
            
        }
        public string GetName
        {
            get { return Name; }
        }
        public string GetSurname
        {
            get { return Surname; }
        }
        public Sex GetSex
        {
            get { return Sex; }
        }
        public string GetAddress
        {
            get { return Address; }
        }
        public string GetNationality
        {
            get { return Nationality; }
        }
        public DateTime GetBirthDate
        {
            get { return BirthDate; }
        }
        public int GetMinAge
        {
            get { return MinAge; }
        }
        public int GetMaxAge
        {
            get { return MaxAge; }
        }
        public int GetAge
        {
            get {
                DateTime today = DateTime.Today;
                TimeSpan age = today - BirthDate;
                double ageInYears = age.TotalDays / 365.2425;
                return (int)Math.Truncate(ageInYears);
            }
        }
        public static Person InputPerson()
        {
            //bool result = input.All(Char.IsLetter);


            string input;
            char inputChar;
            bool isValidLetters;

            string name = null;
            string surname = null;
            Sex sex = Sex.Undefined;
            string address = null;
            DateTime birthDate;
            string nationality =null;
            int minAge = 0;
            int maxAge = 0;

            //NAME
      
            Console.Write("Name (50): ");

            input = Console.ReadLine();
            while (!(input.All(Char.IsLetter) && !string.IsNullOrWhiteSpace(input) && input.Length <= 50)) { 
                Console.Write("Incorrect input! \n" + "Name (50): ");
                input = Console.ReadLine();
            }
            name = input;
    
            //SURNAME
 
            Console.Write("Surname (80): ");

            input = Console.ReadLine();
            while (!(input.All(Char.IsLetter) && !string.IsNullOrWhiteSpace(input) && input.Length <= 80))
            {
                
            Console.Write("Incorrect input! \n" + "Surname (80): ");
            input = Console.ReadLine();
            }
            surname = input;


            //SEX

            Console.Write("Sex (m/f): ");
            inputChar = Console.ReadKey().KeyChar;

            while (!(Char.ToLower(inputChar) == 'm' || Char.ToLower(inputChar) == 'f'))
            {
                Console.Write("Incorrect input! \n" + "Sex (m/f): ");
                inputChar = Console.ReadKey().KeyChar;
            }
           
            if (Char.ToLower(inputChar) == 'm')
            {
                sex = Sex.Male;
            }
            else
            {
                sex = Sex.Female;
            }
            

            //ADDRESS
            Console.WriteLine();
            Console.Write("Address (120): ");
            input = Console.ReadLine();

            while (!(!string.IsNullOrWhiteSpace(input) && input.Length <= 120))
            {

                Console.Write("Incorrect input! \n" + "Address (120): ");
                input = Console.ReadLine();
            }
            address = input;


            //BIRTH DATE
 
            Console.Write("Birth date (dd.MM.yyyy): ");
            input = Console.ReadLine();

            while (!DateTime.TryParse(input, out birthDate))
                Console.Write("Incorrect input! \n" + "Birth date (dd.MM.yyyy): ");

            //NATIONALITY

            Console.Write("Nationality (eg CZE, USA): ");
            input = Console.ReadLine();
            isValidLetters = input.All(Char.IsLetter);

            while (!(input.All(Char.IsLetter) && !string.IsNullOrWhiteSpace(input) && input.Length <= 3))
            {
                Console.Write("Incorrect input! \n" + "Nationality (eg CZE, USA): ");
                input = Console.ReadLine();
            }
            nationality = input.ToUpper();


            //MIN AGE
      
            Console.Write("Minimal apparent age: ");
            input = Console.ReadLine();

            while (!int.TryParse(input, out minAge))
                Console.Write("Incorrect input! \n" + "Minimal apparent age: ");

            //MAX AGE
         
            Console.Write("Maximal apparent age: ");
            input = Console.ReadLine();

            while (!int.TryParse(input, out maxAge))
                Console.Write("Incorrect input! \n" + "Maximal apparent age: ");

            return new Person(name, surname, sex, address, birthDate, nationality, minAge, maxAge);





        }
        public int generateID()
        {
           
            return Identification.ID++;
        }



    }
}
