﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using ConsoleTables;
using PoliceDatabase.Person;

namespace PoliceDatabase.Person
{
    public class MissingPerson : Person
    {
  
        public DateTime MissingDate { get; set; }
        public static List<MissingPerson> MissingPersons = new List<MissingPerson>();

        public MissingPerson(string name, string surname, Sex sex, string address, string nationality, DateTime birthDate, int minAge, int maxAge, DateTime missingDate) : base(name, surname, sex, address, birthDate, nationality, minAge, maxAge)
        {
       
            this.MissingDate = missingDate;
            MissingPersons.Add(this);
        }
        public MissingPerson(Person person, DateTime missingDate) : base(person.GetName, person.GetSurname, person.GetSex, person.GetAddress, person.GetBirthDate, person.GetNationality, person.GetMinAge, person.GetMaxAge)
        {
            
            this.MissingDate = missingDate;
            MissingPersons.Add(this);
        }
        public MissingPerson() : base(null, null, Sex.Undefined, null, new DateTime(2001, 5, 12), null, 0, 0)
        {

        }
        public static void InputMissingPerson(Person person)
        {
            DateTime missingDate;

            //MISSING DATE

            Console.Write("Missing from date (dd.MM.yyyy): ");
            string input = Console.ReadLine();


            while (!DateTime.TryParse(input, out missingDate))
                Console.Write("Incorrect input! \n" + "Missing from date (dd.MM.yyyy): ");

            MissingPerson missPerson = new MissingPerson(person, missingDate);

        }
        public static ConsoleTable GetTable()
        {
            ConsoleTable table = new ConsoleTable("ID", "Name", "Surname", "Sex", "Address", "Birth date (age)", "Nationality", "Apparant age", "Missing from");
            foreach (var missingPerson in MissingPersons)
            {
                table.AddRow(missingPerson.ID, missingPerson.Name, missingPerson.Surname, missingPerson.Sex, missingPerson.Address, missingPerson.BirthDate.ToString("dd.MM.yyyy") + " (" + missingPerson.GetAge + ")", missingPerson.Nationality, missingPerson.MinAge + "-" + missingPerson.MaxAge, missingPerson.MissingDate.ToString("dd.MM.yyyy"));
            }
            return table;
        }
        public static void OutputXML()
        {
            Directory.CreateDirectory(@"../SAVED_XML");

            XmlSerializer xs = new XmlSerializer(typeof(List<MissingPerson>));
            TextWriter tw = new StreamWriter(@"../SAVED_XML/MissingPersons.xml");
            xs.Serialize(tw, MissingPersons);
            tw.Flush();
            tw.Close();


        }
        public static void LoadXML()
        {
            using (var sr = new StreamReader(@"../LOAD_XML/MissingCriminals.xml"))
            {
                XmlSerializer xs = new XmlSerializer(typeof(List<MissingPerson>));
                MissingPersons = (List<MissingPerson>)xs.Deserialize(sr);
            }
        }
    }
}
