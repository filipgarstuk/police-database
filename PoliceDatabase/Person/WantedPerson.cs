﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using ConsoleTables;
using PoliceDatabase.Person;
namespace PoliceDatabase.Person
{
    public class WantedPerson : Person
    {
        //public int ID { get; set; }
        public DateTime WantedDate { get; set; }
        public string WeaponUsed { get; set; }
        public static List<WantedPerson> WantedPersons = new List<WantedPerson>();
        public WantedPerson(string name, string surname, Sex sex, string address, string nationality, DateTime birthDate, int minAge, int maxAge, DateTime wantedDate, string weaponUsed) : base(name, surname, sex, address, birthDate, nationality, minAge, maxAge)
        {
           // ID = generateID();
            this.WantedDate = wantedDate;
            this.WeaponUsed = weaponUsed;
            WantedPersons.Add(this);
        }
        public WantedPerson(Person person, DateTime wantedDate, string weaponUsed) : base(person.GetName, person.GetSurname, person.GetSex, person.GetAddress, person.GetBirthDate, person.GetNationality, person.GetMinAge, person.GetMaxAge)
        {
            //ID = generateID();
            this.WantedDate = wantedDate;
            this.WeaponUsed = weaponUsed;
            WantedPersons.Add(this);
        }

        public WantedPerson() : base(null, null, Sex.Undefined, null, new DateTime(2001, 5, 12), null, 0, 0)
        {
          
        }


        public static void InputWantedPerson(Person person)
        {
            DateTime wantedDate;
            string weaponUsed;

            //WANTED DATE
      
            Console.Write("Wanted from date (dd.MM.yyyy): ");
            string input = Console.ReadLine();

            while (!DateTime.TryParse(input, out wantedDate))
                Console.Write("Incorrect input! \n" + "Wanted from date (dd.MM.yyyy): ");


            //WEAPON USED

            Console.Write("Weapon used (25)(optional): ");
            input = Console.ReadLine();

            while (!(input.Length <= 25))
            {

                Console.Write("Incorrect input! \n" + "Weapon used (25)(optional): ");
                input = Console.ReadLine();
            }

            weaponUsed = input;

            WantedPerson wantedPerson = new WantedPerson(person, wantedDate, weaponUsed);

          
        }
        public static ConsoleTable GetTable()
        {
            ConsoleTable table = new ConsoleTable("ID", "Name", "Surname", "Sex", "Address", "Birth date (age)", "Nationality", "Apparant age", "Wanted from", "Weapon used");
            foreach (var wantedPerson in WantedPersons)
            {
                table.AddRow(wantedPerson.ID, wantedPerson.Name, wantedPerson.Surname, wantedPerson.Sex, wantedPerson.Address, wantedPerson.BirthDate.ToString("dd.MM.yyyy")+" ("+wantedPerson.GetAge+")", wantedPerson.Nationality, wantedPerson.MinAge+"-"+wantedPerson.MaxAge, wantedPerson.WantedDate.ToString("dd.MM.yyyy"), wantedPerson.WeaponUsed);
            }
            return table;
        }
        public static void OutputXML()
        {
            Directory.CreateDirectory(@"../SAVED_XML");

            XmlSerializer xs = new XmlSerializer(typeof(List<WantedPerson>));
            TextWriter tw = new StreamWriter(@"../SAVED_XML/WantedCriminals.xml");
            xs.Serialize(tw, WantedPersons);
            tw.Flush();
            tw.Close();
          

        }
        public static void OutputXMLEncrypted(int numPass)
        {

            OutputXML();

            string password = Login.ComputeSha256Hash(numPass.ToString()).Remove(16);
            

            FileStream fsCrypt = new FileStream(@"../SAVED_XML/WantedCriminals.encrypted", FileMode.Create);

            Byte[] passwordBytes = System.Text.Encoding.UTF8.GetBytes(password);
            


            RijndaelManaged RMCrypto = new RijndaelManaged();

            CryptoStream cs = new CryptoStream(fsCrypt,
                RMCrypto.CreateEncryptor(passwordBytes, passwordBytes),
                CryptoStreamMode.Write);

            FileStream fsIn = new FileStream(@"../SAVED_XML/WantedCriminals.xml", FileMode.Open);

            int data;
            while ((data = fsIn.ReadByte()) != -1)
                cs.WriteByte((byte)data);


            fsIn.Close();
            cs.Close();
            fsCrypt.Close();

        }
        public static void LoadXML()
        {
            
            using (var sr = new StreamReader(@"../LOAD_XML/WantedCriminals.xml"))
            {
                XmlSerializer xs = new XmlSerializer(typeof(List<WantedPerson>));
                WantedPersons = (List<WantedPerson>)xs.Deserialize(sr);
            }
        }
        public static void LoadXMLEncrypted(int numPass) {

            Directory.CreateDirectory(@"../LOAD_XML");

            string password = Login.ComputeSha256Hash(numPass.ToString()).Remove(16);

            Byte[] passwordBytes = System.Text.Encoding.UTF8.GetBytes(password);

            FileStream fsCrypt = new FileStream(@"../LOAD_XML/WantedCriminals.encrypted", FileMode.Open);

            RijndaelManaged RMCrypto = new RijndaelManaged();

            CryptoStream cs = new CryptoStream(fsCrypt,
                RMCrypto.CreateDecryptor(passwordBytes, passwordBytes),
                CryptoStreamMode.Read);

            FileStream fsOut = new FileStream(@"../LOAD_XML/WantedCriminals.xml", FileMode.Create);

            int data;
            while ((data = cs.ReadByte()) != -1)
                fsOut.WriteByte((byte)data);

            fsOut.Close();
            cs.Close();
            fsCrypt.Close();

            LoadXML();
        }
    }
}