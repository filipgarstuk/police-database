﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace PoliceDatabase
{
     public static class Render
    {
        public static void Header(string title)
        {
            Console.WriteLine("---------------------------------");
            Console.WriteLine();
            Console.WriteLine(PadBoth(title,33));
            Console.WriteLine();
            Console.WriteLine("---------------------------------");

        }


        public static void Header()
        {
            Console.WriteLine("---------------------------------");
            Console.WriteLine();
            Console.WriteLine("**Police database version 1.0.0**");
            Console.WriteLine();
            Console.WriteLine("---------------------------------");
            
        }
        public static void Footer()
        {
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("(i) Move using arrows and select using ENTER key");
        }
        public static string PadBoth(this string str, int length)
        {
            int spaces = length - str.Length;
            int padLeft = spaces / 2 + str.Length;
            return str.PadLeft(padLeft).PadRight(length);
        }
        public static void LoggedIn()
        {
            Console.WriteLine("---------------------------------");
            Console.WriteLine(PadBoth("Logged in as: "+ Program.CURRENTLY_LOGGED_IN, 33));
            Console.WriteLine("---------------------------------");
        }



    }
}
