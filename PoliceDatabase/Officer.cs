﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PoliceDatabase
{
    class Officer
    {
        string officerName;
        string officerPassHash;

        public Officer(string officerName, string officerPassHash)
        {
            this.officerName = officerName;
            this.officerPassHash = officerPassHash;
        }
        public string OfficerPassHash
        {
            get { return officerPassHash; }
        }
        public string OfficerName
        {
            get { return officerName; }
        }

    }
}
